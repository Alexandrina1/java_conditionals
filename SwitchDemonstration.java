package com.company;
import java.util.Scanner;
public class SwitchDemonstration
{
    public static void switch1()
    {
        Scanner input = new Scanner(System.in);
         System.out.println("\n Introduceti o cifra de la 1-3");
         int sex = input.nextInt();

         switch (sex)
         {
             case 1:
                 System.out.println("Feminin");
                 break;
             case 2:
                 System.out.println("Masculin");
                 break;
             case 3:
                 System.out.println("Neutru");
                 break;
         }
    }
}